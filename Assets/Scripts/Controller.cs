﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Allows hiding content without disabling script
/// </summary>
public class Controller : MonoBehaviour
{
    [SerializeField]
    protected GameObject content;

    public bool Active
    { 
        get { return content.activeSelf; } 
        set { content.SetActive(value); } 
    }
}
