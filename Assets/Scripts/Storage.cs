﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SimpleJSON;

/// <summary>
/// Static data class to store various things
/// </summary>
public static class Storage
{
    // Stub for session token
    public static string Token { get; set; } = null;
    public static string Nickname { get; set; } = null;


    // URL of server
    public static string root { get; set; } = "http://127.0.0.1";

    // Root resource or api
    public static string api { get; set; } = "/api";

    // Append resource uri to server url
    public static string makeURL(string to)
    {
        return root + to;
    }

    // Node to store main links
    public static JSONNode index { get; set; }
}
