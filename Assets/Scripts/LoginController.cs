﻿using System.Text.RegularExpressions;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

/// <summary>
/// Handles interactions with Log In form
/// </summary>
public class LoginController : Controller
{
    [SerializeField]
    private ApiHandler apiHandler;

    [SerializeField]
    private MainMenu mainMenu;

    // Input fields
    [SerializeField]
    private GameObject login;
    [SerializeField]
    private GameObject password;
    [SerializeField]
    private GameObject confirmPassword;

    // Buttons
    [SerializeField]
    private GameObject buttonLogin;
    [SerializeField]
    private GameObject buttonBack;

    // Status text
    [SerializeField]
    private TextMeshProUGUI statusText;

    // Easy-access properties
    private string loginStr
    {
        get { return login.GetComponent<TMP_InputField>().text; }
        set { login.GetComponent<TMP_InputField>().text = value; }
    }

    private string passwordStr
    {
        get { return password.GetComponent<TMP_InputField>().text; }
        set { password.GetComponent<TMP_InputField>().text = value; }
    }

    private string confirmStr
    {
        get { return confirmPassword.GetComponent<TMP_InputField>().text; }
        set { confirmPassword.GetComponent<TMP_InputField>().text = value; }
    }

    // If form was opened with "Sign up" button
    private bool isSignup = false;

    // 3-32 characters, at least one letter
    private string nicknamePattern = "^(?=.*[A-Za-z])\\w{3,32}$"; 

    //8-64 characters, requred: digit, uppercase, lowwercase, allowed: special charracters
    private string passwordPattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[\\w@$!%*#?&]{8,64}$"; 

    /// <summary>
    /// Show and activate form
    /// </summary>
    /// <param name="signup">Whether form should be for Sign Up action</param>
    public void show(bool signup = false)
    {
        isSignup = signup;
        Active = true;

        int ypos = -70;
        string btext = "Log In";

        if (signup) // If it's sign up form, we need extra place - for confirm password
        {
            ypos = -140;
            btext = "Sign Up";
        }

        // Set text to button
        buttonLogin.GetComponentInChildren<TextMeshProUGUI>().text = btext;

        // Show or hide confirm password
        confirmPassword.SetActive(signup);

        // Move buttons
        Vector3 loginPos = buttonLogin.transform.localPosition;
        Vector3 backPos = buttonBack.transform.localPosition;

        buttonLogin.transform.localPosition = new Vector3(loginPos.x, ypos, loginPos.z);
        buttonBack.transform.localPosition = new Vector3(backPos.x, ypos, backPos.z);
    }

    /// <summary>
    /// Send Log In/Sign Up request to server
    /// </summary>
    public void onLoginButton()
    {
        if (isSignup)
        {
            apiHandler.SendSignupRequest(loginStr, passwordStr, (int status, string tokenOrMessage) =>
            {
                if (status == 0)
                {
                    Storage.Token = tokenOrMessage;
                    Storage.Nickname = loginStr;

                    mainMenu.loggedIn();
                }

                else if (status == 2)
                    statusText.text = "This username is already taken!";

                else
                    statusText.text = $"An error has occured: {tokenOrMessage}";
            });
        }

        else
        {
            apiHandler.SendLoginRequest(loginStr, passwordStr, (int status, string tokenOrMessage) =>
            {
                if (status == 0)
                {
                    Storage.Token = tokenOrMessage;
                    Storage.Nickname = loginStr;

                    mainMenu.loggedIn();
                }

                else if (status == 2)
                    statusText.text = "Incorrect login or password!";

                else
                    statusText.text = $"An error has occured: {tokenOrMessage}";
            });
        }
    }    

    /// <summary>
    /// Check if Log In/Sign up fields are filled correctly
    /// </summary>
    public void validate(GameObject sender)
    {
        bool nicknameValid = Regex.IsMatch(loginStr, nicknamePattern);
        bool passwordValid = Regex.IsMatch(passwordStr, passwordPattern);
        bool confirmValid = !isSignup || passwordStr == confirmStr;

        // Display error message depending on what's wrong
        if (!nicknameValid)
            statusText.text = "Nickname must be 3-32 characters long, can contain only uppercase " +
                "and lowercase letters, numbers and '_'; must contain at least one letter";

        else if (!passwordValid)
            statusText.text = "Password must have at least one of: uppercase, lowercase, number; be 8-64 characters long";

        else if (!confirmValid)
            statusText.text = "Make sure your passwords match";

        else
            statusText.text = "";

        buttonLogin.GetComponent<Button>().interactable = nicknameValid && passwordValid && confirmValid;
    }

    /// <summary>
    /// Hide and disable form
    /// </summary>
    public void hide()
    {
        Active = false;

        buttonLogin.GetComponent<Button>().interactable = false;

        loginStr = "";
        passwordStr = "";
        confirmStr = "";

        statusText.text = "";
    }
}
