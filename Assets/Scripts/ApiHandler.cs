﻿using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.Networking;

using SimpleJSON;

/// <summary>
/// Handles requests to server through REST API
/// </summary>
public class ApiHandler : MonoBehaviour
{
    /// <summary>
    /// Callback for auth functions
    /// </summary>
    /// <param name="status">Result status. 0 for success, 1 for technical problem. Other codes forwarded from API</param>
    /// <param name="message">Text returned from API. Either token or error message</param>
    public delegate void AuthCallback(int status, string message);

    void Start()
    {
        DontDestroyOnLoad(gameObject);

        // Request index resource
        StartCoroutine(GetIndex());
    }

    /// <summary>
    /// Retrieves index "page" of the API
    /// </summary>
    private IEnumerator GetIndex()
    {
        // Create request
        UnityWebRequest request = UnityWebRequest.Get(Storage.makeURL(Storage.api));
        
        // And send it
        yield return request.SendWebRequest();

        // If error - log it
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError(request.error);
            yield break;
        }

        // Parse returned json to node
        Storage.index = JSON.Parse(request.downloadHandler.text);

        // Print one of the links - to test
        Debug.Log($"Connected successfully");
    }

    /// <summary>
    /// Send request to log in to the API
    /// </summary>
    /// <param name="nickname">Username</param>
    /// <param name="password">Password</param>
    /// <param name="callback">Callback function to call on success or failure</param>
    public void SendLoginRequest(string nickname, string password, AuthCallback callback)
    {
        StartCoroutine(PostAuth("login", nickname, password, callback));
    }

    /// <summary>
    /// Send request to sign up to the API
    /// </summary>
    /// <param name="nickname">Username</param>
    /// <param name="password">Password</param>
    /// <param name="callback">Callback function to call on success or failure</param>
    public void SendSignupRequest(string nickname, string password, AuthCallback callback)
    {
        StartCoroutine(PostAuth("signup", nickname, password, callback));
    }

    /// <summary>
    /// Send authentification POST request to given endpoint
    /// Packs nickname and password into request body
    /// Calls callback function on result or error
    /// </summary>
    /// <param name="endpoint">"Page" of api to send to</param>
    /// <param name="nickname">Username</param>
    /// <param name="password">Password</param>
    /// <param name="callback">Function to call on success or failure</param>
    private IEnumerator PostAuth(string endpoint, string nickname, string password, AuthCallback callback)
    {
        // Put nickname and password to new JSONObject
        JSONNode body = packLoginInfo(nickname, password);

        // Make request to post it to login url
        UnityWebRequest request = PostJSON(Storage.makeURL(Storage.index[endpoint]), body);

        // Send it
        yield return request.SendWebRequest();

        // Check for errors
        if (request.isNetworkError || request.isHttpError)
        {
            callback(1, "Connection error");
            yield break;
        }

        // Parse response
        JSONNode response = JSON.Parse(request.downloadHandler.text);

        if (response["status"] == 0)
            callback(0, response["token"]); // Auth successful
        else
            callback(response["status"], response["message"]);  // Auth failed
    }

    /// <summary>
    /// Packs login info into json object
    /// </summary>
    /// <param name="nickname">Username</param>
    /// <param name="password">Password</param>
    private JSONObject packLoginInfo(string nickname, string password)
    {
        JSONObject obj = new JSONObject();
        obj["nickname"] = new JSONString(nickname);
        obj["password"] = new JSONString(password);

        return obj;
    }

    /// <summary>
    /// Create POST request with JSON as a body
    /// </summary>
    /// <param name="url"></param>
    /// <param name="json"></param>
    private UnityWebRequest PostJSON(string url, JSONNode json)
    {
        UnityWebRequest r = new UnityWebRequest(url)
        {
            uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(json.ToString())),
            downloadHandler = new DownloadHandlerBuffer(),
            method = UnityWebRequest.kHttpVerbPOST
        };

        r.SetRequestHeader("Content-Type", "application/json");

        return r;
    }
}
