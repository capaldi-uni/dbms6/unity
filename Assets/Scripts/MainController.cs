﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

/// <summary>
/// Handles actions on Main Menu panel
/// </summary>
public class MainController : Controller
{
    [SerializeField]
    private GameObject logSignIn;

    [SerializeField]
    private GameObject logOut;

    [SerializeField]
    private TextMeshProUGUI statusText;

    /// <summary>
    /// Show and activate main screen
    /// </summary>
    public void show()
    {
        bool loggedIn = Storage.Nickname != null;

        string nicknameDisplay;

        logSignIn.SetActive(!loggedIn);
        logOut.SetActive(loggedIn);

        if (!loggedIn)
        {
            nicknameDisplay = "Not logged in";
        }
        else
        {
            nicknameDisplay = $"Logged in as {Storage.Nickname}";
        }

        statusText.text = nicknameDisplay;

        Active = true;
    }

    /// <summary>
    /// Hide main screen
    /// </summary>
    public void hide()
    {
        Active = false;
    }
}
