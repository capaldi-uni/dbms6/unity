﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

/// <summary>
/// Coordinates interactions of main menu elements
/// </summary>
public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private MainController main;

    [SerializeField]
    private LoginController login;

    void Start()
    {
        main.show();
        login.hide();
    }

    /// <summary>
    /// Placeholder for play button pressed handler
    /// </summary>
    public void onPlay()
    {
        Debug.Log("Play pressed");
    }

    /// <summary>
    /// Log In button press
    /// </summary>
    public void onLogin()
    {
        login.show();
        main.hide();
    }

    /// <summary>
    /// Sign Up button press
    /// </summary>
    public void onSignup()
    {
        login.show(true);
        main.hide();
    }

    /// <summary>
    /// Log out button press
    /// </summary>
    public void onLogout()
    {
        Storage.Token = null;
        Storage.Nickname = null;

        main.show();
    }

    /// <summary>
    /// Back button press
    /// </summary>
    public void onBack()
    {
        main.show();
        login.hide();
    }

    /// <summary>
    /// Exit button press
    /// </summary>
    public void onExit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Called when user logs in
    /// </summary>
    public void loggedIn()
    {
        main.show();
        login.hide();
    }
}
